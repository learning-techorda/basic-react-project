import react from 'react';

interface ButtonProp {
	className?: string;
	children?: react.ReactNode;
	onClick?: (event: react.MouseEvent<HTMLButtonElement>) => void;
	type?: 'button' | 'submit' | 'reset';
	imagePath?: string;
	altText?: string;
}

const Button: react.FC<ButtonProp> = ({
	className,
	children,
	onClick,
	imagePath,
	type,
	altText = 'Button image',
}) => {
	return (
		<button className={className} onClick={onClick} type={type}>
			{imagePath && (
				<img
					src={imagePath}
					alt={altText}
					style={{ width: '24px', height: '24px' }}
				/>
			)}
			{children}
		</button>
	);
};

export default Button;
