import React from 'react';

export interface InputProps {
	htmlFor?: string;
	labelText?: string;
	inputType?: string;
	placeholderText?: string;
	onChange?: React.ChangeEventHandler<HTMLInputElement>;
	showError?: boolean;
	errorMessage?: string | null | undefined;
	className?: string;
	min?: string;
	max?: string;
	name?: string;
	value?: string | number;
	type?: string | number;
}

const Input: React.FC<InputProps> = ({
	htmlFor,
	labelText,
	inputType,
	placeholderText,
	onChange,
	showError,
	errorMessage,
	className,
	value,
}) => {
	let effectiveErrorMessage = errorMessage;

	if (!effectiveErrorMessage && showError && labelText) {
		effectiveErrorMessage = `${labelText[0].toUpperCase()}${labelText.slice(1)} is required.`;
	} else if (
		showError &&
		effectiveErrorMessage &&
		effectiveErrorMessage.trim().length < 2 &&
		labelText
	) {
		effectiveErrorMessage = `${labelText[0].toUpperCase()}${labelText.slice(1)} must be at least 2 characters.`;
	}

	const inputClass = `${className} ${showError ? 'input-error' : ''}`.trim();

	return (
		<>
			<label className='label' htmlFor={htmlFor}>
				{!labelText ? '' : labelText + `:`}
			</label>
			<input
				className={inputClass}
				type={inputType}
				id={htmlFor}
				name={htmlFor}
				placeholder={placeholderText}
				onChange={onChange}
				value={value}
			/>
			{showError && errorMessage && (
				<div className='error-message'>{errorMessage}</div>
			)}
		</>
	);
};

export default Input;
