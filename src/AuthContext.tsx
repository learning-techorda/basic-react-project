import React, {
	createContext,
	useContext,
	useState,
	ReactNode,
	ReactElement,
	useEffect,
} from 'react';

interface User {
	name: string;
	email: string;
}

interface AuthContextType {
	user: User | null;
	saveUser: (user: User) => void;
	logout: () => void;
}

const AuthContext = createContext<AuthContextType | null>(null);

interface AuthProviderProps {
	children: ReactNode;
}

export const AuthProvider: React.FC<AuthProviderProps> = ({
	children,
}): ReactElement | null => {
	const [user, setUser] = useState<User | null>(null);

	useEffect(() => {
		const userFromStorage = localStorage.getItem('user');
		if (userFromStorage) {
			setUser(JSON.parse(userFromStorage));
		}
	}, []);

	const saveUser = (user: User) => {
		localStorage.setItem('user', JSON.stringify(user));
		setUser(user);
	};

	const logout = () => {
		localStorage.removeItem('userToken');
		localStorage.removeItem('user');
		setUser(null);
	};

	return (
		<AuthContext.Provider value={{ user, saveUser, logout }}>
			{children}
		</AuthContext.Provider>
	);
};

export const useAuth = (): AuthContextType => {
	const context = useContext(AuthContext);
	if (!context) {
		throw new Error('useAuth must be used within an AuthProvider');
	}
	return context;
};
