export const fetchCoursesApi = async () => {
	console.log('Request /courses/all... works');
	const response = await fetch('http://localhost:4000/courses/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Cache-Control': 'no-cache',
		},
	});

	console.log('fetchCoursesApi status:', response.status);

	if (!response.ok) {
		throw new Error('Network response was not ok');
	}

	const data = await response.json();
	console.log('Received courses data:', data);
	return data;
};

export const fetchAuthorsApi = async () => {
	console.log('Request /authors/all... works');
	const response = await fetch('http://localhost:4000/authors/all', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Cache-Control': 'no-cache',
		},
	});

	console.log('fetchAuthorsApi status:', response.status);

	if (!response.ok) {
		throw new Error('Network response was not ok');
	}

	const data = await response.json();
	console.log('Received authors data:', data);
	return data;
};

export const addCourseApi = async (course) => {
	console.log('Request /courses/add... works');
	const response = await fetch('http://localhost:4000/courses/add', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Cache-Control': 'no-cache',
			Authorization: localStorage.getItem('userToken'),
		},
		body: JSON.stringify(course),
	});

	console.log('addCourseApi status:', response.status);

	if (!response.ok) {
		throw new Error('Network response was not ok');
	}

	const data = await response.json();
	console.log('Received added course data:', data);
	return data;
};

export const addAuthorApi = async (authorName) => {
	console.log('Request /authors/add... works');
	const response = await fetch('http://localhost:4000/authors/add', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Cache-Control': 'no-cache',
			Authorization: localStorage.getItem('userToken'),
		},
		body: JSON.stringify({ name: authorName }),
	});

	console.log('addAuthorApi status:', response.status);

	if (!response.ok) {
		throw new Error('Network response was not ok');
	}

	const data = await response.json();
	console.log('Received added author data:', data);
	return data;
};

export const deleteAuthorApi = async (authorId) => {
	const response = await fetch(`http://localhost:4000/authors/${authorId}`, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			'Cache-Control': 'no-cache',
			Authorization: localStorage.getItem('userToken'),
		},
	});

	if (!response.ok) {
		throw new Error('Network response was not ok');
	}

	return await response.json();
};

export const deleteCourseApi = async (courseId) => {
	const response = await fetch(`http://localhost:4000/courses/${courseId}`, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
			'Cache-Control': 'no-cache',
			Authorization: localStorage.getItem('userToken'),
		},
	});

	if (!response.ok) {
		throw new Error('Network response was not ok');
	}

	return await response.json();
};

export const fetchUsersApi = async () => {
	console.log('Request /authors/all... works');
	const response = await fetch('http://localhost:4000/users/me', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Cache-Control': 'no-cache',
			Authorization: localStorage.getItem('userToken'),
		},
	});

	console.log('fetchAuthorsApi status:', response.status);

	if (!response.ok) {
		throw new Error('Network response was not ok');
	}

	const data = await response.json();
	console.log('Received authors data:', data);
	return data;
};

export const updateCourseApi = async (courseId, course) => {
	console.log('Request to /courses/update... works');
	const response = await fetch(`http://localhost:4000/courses/${courseId}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Cache-Control': 'no-cache',
			Authorization: localStorage.getItem('userToken'),
		},
		body: JSON.stringify(course),
	});

	console.log('updateCourseApi status:', response.status);

	if (!response.ok) {
		throw new Error('Network response was not ok');
	}

	const data = await response.json();
	console.log('Received updated course data:', data);
	return data;
};
