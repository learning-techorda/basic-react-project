export interface CourseType {
	id?: string | undefined;
	title: string;
	description: string;
	duration: number;
	creationDate?: string;
	authors: string[];
}

export interface AuthorsType {
	id: string;
	name: string;
}
