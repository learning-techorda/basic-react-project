import React, { FC } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import CourseInfo from 'src/components/CourseInfo/CourseInfo';
import { RootState } from 'src/store';

const CourseInfoPage: FC = () => {
	const { courseId } = useParams();
	const navigate = useNavigate();

	const course = useSelector((state: RootState) =>
		state.courses.find((course) => course.id === courseId)
	);

	const authors = useSelector((state: RootState) => state.authors);

	if (course) {
		const courseAuthors = authors.filter((author) =>
			course.authors.includes(author.id)
		);

		const handleBack = () => {
			navigate('/courses');
		};

		return (
			<CourseInfo course={course} authors={courseAuthors} onBack={handleBack} />
		);
	}

	return null;
};

export default CourseInfoPage;
