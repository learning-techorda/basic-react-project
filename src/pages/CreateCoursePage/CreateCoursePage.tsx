import React, { FC } from 'react';
import CourseForm from 'src/components/CourseForm/CourseForm';
import { useNavigate } from 'react-router-dom';

export const CreateCoursePage: FC = () => {
	const navigate = useNavigate();
	const navigateBack = () => navigate(-1);

	return <CourseForm navigateBack={navigateBack} />;
};
