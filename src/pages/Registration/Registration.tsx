import Input from '../../сommon/Input/input';
import Button from '../../сommon/Button/Button';
import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import { useRegistrationForm } from './useRegistrationForm';
import './Registration.sass';

const Registration: FC = () => {
	const { handleChange, nameError, emailError, passwordError, handleSubmit } =
		useRegistrationForm();

	return (
		<div className='registration'>
			<h1 className='registration-container-heading'>Registration</h1>
			<div className='registration-container'>
				<form className='registration-container-form' onSubmit={handleSubmit}>
					<Input
						htmlFor='name'
						labelText='Name'
						inputType='text'
						placeholderText='Enter your name'
						onChange={handleChange}
						showError={!!nameError}
						errorMessage={nameError}
						className='input-name'
					/>
					<Input
						htmlFor='email'
						labelText='Email'
						inputType='email'
						placeholderText='Enter your email'
						onChange={handleChange}
						showError={!!emailError}
						errorMessage={emailError}
						className='input-email'
					/>
					<Input
						htmlFor='password'
						labelText='Password'
						inputType='password'
						placeholderText='Enter your password'
						onChange={handleChange}
						showError={!!passwordError}
						errorMessage={passwordError}
						className='input-password'
					/>
					<Button className='btn-registration' type='submit'>
						Register
					</Button>
					<p className='registration-form-message'>
						If you have an account you may{' '}
						<Link className='registration-form-message-link' to='/login'>
							Login
						</Link>
					</p>
				</form>
			</div>
			;
		</div>
	);
};
export default Registration;
