import { useState } from 'react';
import { validateField } from 'src/helpers/validators';
import { useNavigate } from 'react-router-dom';

export function useRegistrationForm() {
	const [attemptedSubmit] = useState(false);

	const [name, setName] = useState('');
	const [nameError, setNameError] = useState('');

	const [email, setEmail] = useState('');
	const [emailError, setEmailError] = useState('');

	const [password, setPassword] = useState('');
	const [passwordError, setPasswordError] = useState('');

	const navigate = useNavigate();

	const handleChange = (event: { target: { name: string; value: string } }) => {
		const { name, value } = event.target;
		console.log(`Handling change for ${name} with new value: '${value}'`);
		console.log('New input value:', event.target.value);

		switch (name) {
			case 'name':
				setName(value);
				setNameError(validateField('Name', value));
				break;
			case 'email':
				setEmail(value);
				setEmailError(validateField('Email', value));
				break;
			case 'password':
				setPassword(value);
				setPasswordError(validateField('Password', value));
				break;
			default:
				break;
		}
	};

	const handleSubmit = async (event: { preventDefault: () => void }) => {
		event.preventDefault();

		const errors: Record<string, string> = {};
		const tmpNameError = validateField('Name', name);
		if (tmpNameError) {
			errors.name = tmpNameError;
			setNameError(tmpNameError);
		}

		const tmpEmailError = validateField('Email', email);
		if (tmpEmailError) {
			errors.email = tmpEmailError;
			setEmailError(tmpEmailError);
		}

		const tmpPasswordError = validateField('Password', password);
		if (tmpPasswordError) {
			errors.password = tmpPasswordError;
			setPasswordError(tmpPasswordError);
		}

		if (Object.keys(errors).length === 0) {
			const newUser = {
				name,
				email,
				password,
			};

			try {
				const response = await fetch('http://localhost:4000/register', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(newUser),
				});

				await response.json();

				if (response.ok) {
					navigate('/login');
				} else {
					const errorData = await response.json();
					throw new Error(
						errorData.errors
							? errorData.errors.join(', ')
							: 'Registration failed'
					);
				}
			} catch (error) {
				console.error('Error:', error);
			}
		}
	};
	return {
		name,
		setName,
		nameError,
		email,
		setEmail,
		emailError,
		password,
		setPassword,
		passwordError,
		attemptedSubmit,
		handleChange,
		handleSubmit,
	};
}
