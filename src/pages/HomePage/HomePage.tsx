import React, { FC, useEffect } from 'react';
import Courses from 'src/components/Courses/Courses';
import EmptyCourseList from 'src/components/EmptyCourseList/EmptyCourseList';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCourses } from 'src/store/courses/thunk';
import { AppDispatch, RootState } from 'src/store';
import { fetchAuthors } from 'src/store/authors/thunk';

const HomePage: FC = () => {
	const dispatch = useDispatch<AppDispatch>();

	useEffect(() => {
		dispatch(fetchCourses());
		dispatch(fetchAuthors());
	}, [dispatch]);

	const courses = useSelector((state: RootState) => state.courses);
	const authors = useSelector((state: RootState) => state.authors);

	if (courses.length > 0) {
		return (
			<Courses
				courses={courses}
				authors={authors}
				onAuthorRemoved={function (): void {
					throw new Error('Function not implemented.');
				}}
			/>
		);
	}

	return <EmptyCourseList />;
};

export default HomePage;
