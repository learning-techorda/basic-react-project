import { useState } from 'react';
import { validateField } from 'src/helpers/validators';
import { useNavigate } from 'react-router-dom';
import { useAuth } from 'src/AuthContext';
import { useDispatch } from 'react-redux';
import { loginSuccess } from 'src/store/user/actions';
import { fetchCurrentUser } from '../../store/user/thunk';
import { AppDispatch } from '../../store';

export function useLoginForm() {
	const dispatch = useDispatch<AppDispatch>();

	const [email, setEmail] = useState('');
	const [emailError, setEmailError] = useState('');

	const [password, setPassword] = useState('');
	const [passwordError, setPasswordError] = useState('');

	const navigate = useNavigate();
	const { saveUser } = useAuth();

	const handleChange = (event: { target: { name: string; value: string } }) => {
		const { name, value } = event.target;

		switch (name) {
			case 'email':
				setEmail(value);
				setEmailError(validateField('Email', value));
				break;
			case 'password':
				setPassword(value);
				setPasswordError(validateField('Password', value));
				break;
			default:
				break;
		}
	};

	const handleSubmit = async (event: { preventDefault: () => void }) => {
		event.preventDefault();

		const errors: Record<string, string> = {};

		const tmpEmailError = validateField('Email', email);
		if (tmpEmailError) {
			errors.email = tmpEmailError;
			setEmailError(tmpEmailError);
		}

		const tmpPasswordError = validateField('Password', password);
		if (tmpPasswordError) {
			errors.password = tmpPasswordError;
			setPasswordError(tmpPasswordError);
		}

		const loginData = {
			email: email,
			password: password,
		};
		try {
			const response = await fetch('http://localhost:4000/login', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(loginData),
			});

			const data = await response.json();

			if (response.ok) {
				localStorage.setItem('userToken', data.result);
				saveUser(data.user);
				const payload = {
					name: data.user.name,
					email: data.user.email,
					token: data.result,
				};
				dispatch(loginSuccess(payload));
				dispatch(fetchCurrentUser());

				navigate('/courses');
			} else {
				throw new Error(data.message || 'Login failed');
			}
		} catch (error) {
			console.error('Login error:', error);
		}
	};
	return {
		email,
		setEmail,
		emailError,
		password,
		setPassword,
		passwordError,
		handleChange,
		handleSubmit,
	};
}
