import React, { FC } from 'react';
import Input from '../../сommon/Input/input';
import { useLoginForm } from './useLoginForm';
import { Link } from 'react-router-dom';
import Button from 'src/сommon/Button/Button';
import '../Registration/Registration.sass';

const Login: FC = () => {
	const { handleChange, emailError, passwordError, handleSubmit } =
		useLoginForm();

	return (
		<div className='registration'>
			<h1 className='registration-container-heading'>Login</h1>
			<div className='registration-container'>
				<form className='registration-container-form' onSubmit={handleSubmit}>
					<Input
						htmlFor='email'
						labelText='Email'
						inputType='email'
						placeholderText='Input text'
						onChange={handleChange}
						showError={!!emailError}
						errorMessage={emailError}
						className='input-email'
					/>
					<Input
						htmlFor='password'
						labelText='Password'
						inputType='password'
						placeholderText='Input text'
						onChange={handleChange}
						showError={!!passwordError}
						errorMessage={passwordError}
						className='input-email'
					/>
					<Button className='btn-registration' type='submit'>
						Login
					</Button>
					<p className='registration-form-message'>
						If you don't have an account you may{' '}
						<Link className='registration-form-message-link' to='/registration'>
							<p className='login-message'>Registration</p>
						</Link>
					</p>
				</form>
			</div>
		</div>
	);
};
export default Login;
