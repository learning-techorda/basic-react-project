import { createAsyncThunk } from '@reduxjs/toolkit';
import { addAuthorApi, deleteAuthorApi, fetchAuthorsApi } from '../../services';
import { removeAuthor, setAuthors } from './reducer';

export const fetchAuthors = createAsyncThunk(
	'authors/fetchAuthors',
	async (_, { dispatch }) => {
		const response = await fetchAuthorsApi();
		if (response.successful) {
			dispatch(setAuthors(response.result));
			return response.result;
		} else {
			throw new Error('Failed to fetch authors');
		}
	}
);

export const addAuthor = createAsyncThunk(
	'authors/addAuthor',
	async (authorName: string, { dispatch }) => {
		try {
			const addedAuthor = await addAuthorApi(authorName);
			dispatch(addAuthor(addedAuthor));
			const authors = await fetchAuthorsApi();
			dispatch(setAuthors(authors.result));

			return addedAuthor;
		} catch (error) {
			console.error('Failed to add author:', error);
			throw error;
		}
	}
);

export const deleteAuthor = createAsyncThunk(
	'authors/deleteAuthor',
	async (authorId: string, { dispatch }) => {
		await deleteAuthorApi(authorId);
		dispatch(removeAuthor(authorId));
		return authorId;
	}
);

export { removeAuthor };
