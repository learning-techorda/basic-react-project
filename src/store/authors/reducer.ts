import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Authors } from './types';

const counterAuthorSlice = createSlice({
	name: 'authors',
	initialState: [] as Authors[],
	reducers: {
		setAuthors(state, action: PayloadAction<Authors[]>) {
			return action.payload;
		},
		addAuthor(state, action: PayloadAction<Authors>) {
			console.log('Before adding author:', state);
			state.push(action.payload);
			console.log('After adding author:', state);
		},
		removeAuthor(state, action: PayloadAction<string>) {
			return state.filter((author) => author.id !== action.payload);
		},
	},
});

export default counterAuthorSlice.reducer;
export const { setAuthors, addAuthor, removeAuthor } =
	counterAuthorSlice.actions;
