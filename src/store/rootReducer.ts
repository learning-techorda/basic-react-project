import { combineReducers } from 'redux';
import counterAuthorSlice from './authors/reducer';
import counterCourseSlice from './courses/reducer';
import userReducer from './user/reducer';

const rootReducer = combineReducers({
	user: userReducer,
	courses: counterCourseSlice,
	authors: counterAuthorSlice,
});

export default rootReducer;
