export interface UserPayload {
	name: string;
	email: string;
	token: string;
	role?: string;
}

export interface UserState {
	isAuth: boolean;
	name: string;
	email: string;
	token: string;
	role: string;
}

export interface RootState {
	user: UserState;
}

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT = 'LOGOUT';

export type UserAction =
	| { type: typeof LOGIN_SUCCESS; payload: UserPayload }
	| { type: typeof LOGOUT };
