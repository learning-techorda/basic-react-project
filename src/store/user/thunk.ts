import { fetchUsersApi } from '../../services';
import { loginSuccess } from './actions';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { UserPayload } from './types';

export const fetchCurrentUser = createAsyncThunk(
	'user/fetchCurrentUser',
	async (_, { dispatch }) => {
		try {
			const data = await fetchUsersApi();
			if (data) {
				dispatch(
					loginSuccess(<UserPayload>{
						role: data.result.role,
					})
				);
			}
		} catch (error) {
			console.error('Error fetching user details:', error);
		}
	}
);
