import { LOGIN_SUCCESS, LOGOUT, UserAction, UserState } from './types';

const initialState: UserState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
	role: '',
};

const userReducer = (state = initialState, action: UserAction) => {
	switch (action.type) {
		case LOGIN_SUCCESS:
			return {
				...state,
				isAuth: true,
				...action.payload,
			};
		case LOGOUT:
			return initialState;
		default:
			return state;
	}
};
export default userReducer;
