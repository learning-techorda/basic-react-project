import { LOGIN_SUCCESS, LOGOUT, UserAction, UserPayload } from './types';

export const loginSuccess = (userData: UserPayload): UserAction => ({
	type: LOGIN_SUCCESS,
	payload: userData,
});

export const logout = (): UserAction => ({
	type: LOGOUT,
});
