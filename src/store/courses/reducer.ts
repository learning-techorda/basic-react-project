import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Course } from './types';

const counterCourseSlice = createSlice({
	name: 'courses',
	initialState: [] as Course[],
	reducers: {
		setCourses(state, action: PayloadAction<Course[]>) {
			return action.payload;
		},
		addNewCourse(state, action: PayloadAction<Course>) {
			state.push(action.payload);
		},
		removeCourse(state, action: PayloadAction<string>) {
			return state.filter((course) => course.id !== action.payload);
		},
		updateCurrentCourse(state, action: PayloadAction<Course>) {
			const index = state.findIndex(
				(course) => course.id === action.payload.id
			);
			state[index] = action.payload;
		},
	},
});

export default counterCourseSlice.reducer;
export const { setCourses, addNewCourse, removeCourse, updateCurrentCourse } =
	counterCourseSlice.actions;
