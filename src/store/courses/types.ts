export interface Course {
	id?: string;
	title: string;
	description: string;
	creationDate?: string;
	duration: number;
	authors: string[];
}

export interface UpdateCourse {
	courseId: string;
	course: Course;
}
