import { createAsyncThunk } from '@reduxjs/toolkit';
import {
	addCourseApi,
	fetchCoursesApi,
	deleteCourseApi,
	updateCourseApi,
} from '../../services';
import {
	removeCourse,
	setCourses,
	addNewCourse,
	updateCurrentCourse,
} from './reducer';
import { Course, UpdateCourse } from './types';

export const fetchCourses = createAsyncThunk(
	'courses/fetchCourses',
	async (_, { dispatch }) => {
		const courses = await fetchCoursesApi();
		dispatch(setCourses(courses.result));
		return courses;
	}
);

export const addCourse = createAsyncThunk(
	'courses/addCourse',
	async (course: Course, { dispatch }) => {
		const addedCourse = await addCourseApi(course);
		const courses = await fetchCoursesApi();
		dispatch(addNewCourse(addedCourse));
		dispatch(setCourses(courses));
		return addedCourse;
	}
);

export const deleteCourse = createAsyncThunk(
	'courses/deleteCourse',
	async (courseId: string, { dispatch }) => {
		await deleteCourseApi(courseId);
		dispatch(removeCourse(courseId));
		return courseId;
	}
);

export const updateCourse = createAsyncThunk(
	'courses/updateCourse',
	async ({ courseId, course }: UpdateCourse, { dispatch }) => {
		const updatedCourse = await updateCourseApi(courseId, course);
		dispatch(updateCurrentCourse(updatedCourse));
		console.log('Updated course from API:', updatedCourse);
		return updatedCourse;
	}
);
