import { configureStore } from '@reduxjs/toolkit';
// import { userReducer, coursesReducer, authorReducer } from './rootReducer';
import rootReducer from './rootReducer';

const store = configureStore({
	reducer: rootReducer,
	middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;

export default store;
