import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';

import Header from './components/Header/Header';
import Home from './pages/HomePage/HomePage';
import CourseInfoPage from './pages/CourseInfoPage/CourseInfoPage';
import Registration from './pages/Registration/Registration';
import Login from './pages/Login/Login';
import { CreateCoursePage } from './pages/CreateCoursePage/CreateCoursePage';

import './App.sass';
import { RedirectIfAuthenticated } from './components/RedirectIfAuthenticated/RedirectIfAuthenticated';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';

function App() {
	return (
		<div className='App'>
			<div className='App-container'>
				<Header />
				<Routes>
					<Route path='/courses' element={<Home />} />
					<Route
						path='courses/add'
						element={
							<PrivateRoute>
								<CreateCoursePage />
							</PrivateRoute>
						}
					/>
					<Route
						path='/courses/update/:courseId'
						element={
							<PrivateRoute>
								<CreateCoursePage />
							</PrivateRoute>
						}
					/>

					<Route path='/courses/:courseId' element={<CourseInfoPage />} />
					<Route
						path='/registration'
						element={
							<RedirectIfAuthenticated>
								<Registration />
							</RedirectIfAuthenticated>
						}
					/>
					<Route
						path='/login'
						element={
							<RedirectIfAuthenticated>
								<Login />
							</RedirectIfAuthenticated>
						}
					/>
					<Route path='*' element={<Navigate to='/courses' />} />
				</Routes>
			</div>
		</div>
	);
}

export default App;
