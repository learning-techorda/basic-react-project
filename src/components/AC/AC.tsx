import { FC, PropsWithChildren } from 'react';

export const AC: FC<PropsWithChildren> = ({ children }) => {
	return <>{localStorage.getItem('userToken') ? children : undefined}</>;
};
