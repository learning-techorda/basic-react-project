import { FC, PropsWithChildren } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';

export const PrivateRouteButton: FC<PropsWithChildren> = ({ children }) => {
	const userRole = useSelector((state: RootState) => state.user.role);
	if (userRole === 'admin') {
		return <>{children}</>;
	}
	return null;
};
