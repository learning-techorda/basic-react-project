import React, { ReactElement } from 'react';
import { Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';

interface PrivateRouteProps {
	children: ReactElement | string | null;
}

const PrivateRoute: React.FC<PrivateRouteProps> = ({
	children,
}): React.ReactElement | null => {
	const userRole = useSelector((state: RootState) => state.user.role);

	if (!localStorage.getItem('userToken')) {
		return <Navigate to='/login' replace />;
	}

	if (userRole === 'admin') {
		return <>{children}</>;
	} else if (userRole === 'user') {
		return <Navigate to='/courses' replace />;
	}

	return null;
};
export default PrivateRoute;
