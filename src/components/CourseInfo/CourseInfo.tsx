/* eslint-disable @typescript-eslint/no-explicit-any */
import react from 'react';
import { useNavigate } from 'react-router-dom';
import Button from '../../сommon/Button/Button';
import { AuthorsType, CourseType } from '../../types/card';
import getCourseDuration from '../../helpers/getCourseDuration';
import './CourseInfo.sass';
import FormatCreationDate from '../../helpers/formatCreationDate';

interface CourseInfoProps {
	course: CourseType;
	authors: AuthorsType[];
	onBack: () => void;
}

const CourseInfo: react.FC<CourseInfoProps> = ({ course, authors, onBack }) => {
	const authorNames = course.authors
		.map(
			(authorId: string) =>
				authors.find((author) => author.id === authorId)?.name || 'Unknown'
		)
		.join(', ');

	const navigate = useNavigate();

	const handleBack = () => {
		onBack();
		navigate('/courses');
	};

	return (
		<div className='CourseInfo'>
			<div className='CourseInfo-container'>
				<h1 className='CourseInfo-container-heading'>{course.title}</h1>
				<div className='CourseInfo-container-body'>
					<h2 className='CourseInfo-container-body-title'>Description:</h2>
					<div className='CourseInfo-container-block'>
						<div className='CourseInfo-container-body-description'>
							<p className='CourseInfo-description'>{course.description}</p>
						</div>
						<div className='vertical-line'></div>
						<div className='CourseInfo-container-body-data'>
							<div className='CourseInfo-data'>
								<ul>
									<li>
										<strong className='id-label'>ID:</strong>{' '}
										<span className='id-value'> {course.id}</span>
									</li>
									<li>
										<strong className='id-label'>Duration:</strong>{' '}
										<span className='id-value'>
											{getCourseDuration(course.duration)}
										</span>
									</li>
									<li>
										<strong className='id-label'>Created:</strong>{' '}
										<span className='id-value'>
											{FormatCreationDate(course.creationDate)}
										</span>
									</li>
									<li>
										<strong className='id-label'>Authors:</strong>{' '}
										<span className='id-value'>{authorNames}</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div className='btn-container'>
					<Button
						className='common-btn-style btn-card-info'
						onClick={handleBack}
					>
						BACK
					</Button>
				</div>
			</div>
		</div>
	);
};

export default CourseInfo;
