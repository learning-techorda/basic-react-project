import { FC, PropsWithChildren } from 'react';
import { Navigate } from 'react-router-dom';

export const RedirectIfAuthenticated: FC<PropsWithChildren> = ({
	children,
}) => {
	if (localStorage.getItem('userToken')) {
		return <Navigate to='/courses' replace />;
	}

	return <>{children}</>;
};
