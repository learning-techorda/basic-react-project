import React, { FC } from 'react';
import Button from 'src/сommon/Button/Button';
import trash from '../AuthorItem/images-btn/trash-can-regular.svg';
import { Author } from '../CourseForm/useCourseForm';

interface CourseAuthorsProps {
	authors: Author[];
	onRemoveAuthor: (authorName: string) => void;
}

const CourseAuthors: FC<CourseAuthorsProps> = ({ authors, onRemoveAuthor }) => {
	return (
		<div>
			{authors.length > 0 ? (
				authors.map((author, index) => (
					<div key={index}>
						{author.name}
						<Button
							className='author-item-btn-delete'
							imagePath={trash}
							altText='Move Author'
							onClick={(event) => {
								event.preventDefault();
								onRemoveAuthor(author.id);
							}}
						></Button>
					</div>
				))
			) : (
				<p>Author list is empty</p>
			)}
		</div>
	);
};

export default CourseAuthors;
