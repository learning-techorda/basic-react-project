import React from 'react';
import Button from '../../сommon/Button/Button';
import './EmptyCourseList.sass';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';

function EmptyCourseList() {
	const navigate = useNavigate();
	const userRole = useSelector((state: RootState) => state.user.role);
	const handleAddNewCourseClick = () => {
		navigate('/courses/add');
	};

	return (
		<div className='EmptyCourse-container'>
			<h1 className='EmptyCourse-container-title'>Course List is Empty</h1>
			<p className='EmptyCourse-container-subtitle'>
				Please use "Add New Course" button to add your first course
			</p>
			{userRole === 'admin' ? (
				<Button
					className='common-btn-style btn-EmptyCourse'
					onClick={handleAddNewCourseClick}
				>
					ADD NEW COURSE
				</Button>
			) : (
				<p>
					You don't have permissions to create a course. Please log in as ADMIN.
				</p>
			)}
		</div>
	);
}

export default EmptyCourseList;
