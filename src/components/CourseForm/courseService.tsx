interface Course {
	id: string;
	title: string;
	description: string;
	creationDate: string;
	duration: number;
	authors: string[];
}

class CourseService {
	courses: Course[] = [];

	saveCourse(course: Course) {
		if (
			Array.isArray(course.authors) &&
			course.authors.every((id) => typeof id === 'string')
		) {
			this.courses.push(course);
		} else {
			console.error('Invalid author IDs:', course.authors);
		}
	}

	getCourses() {
		return this.courses;
	}
}

const courseService = new CourseService();
export default courseService;
