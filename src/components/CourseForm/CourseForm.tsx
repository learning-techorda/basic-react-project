import React from 'react';
import Button from '../../сommon/Button/Button';
import useCourseForm from './useCourseForm';
import AuthorItem from '../AuthorItem/AuthorItem';
import CourseAuthors from '../CourseAuthors/CourseAuthors';
import MainInfo from './CourseComponents/MainInfo';
import Duration from './CourseComponents/Duration';
import AuthorNameInput from './CourseComponents/AuthorNameInput';
import './CourseForm.sass';

type CreateCourseProps = {
	navigateBack: () => void;
};
const CourseForm: React.FC<CreateCourseProps> = ({ navigateBack }) => {
	const {
		handleChange,
		titleError,
		descriptionError,
		duration,
		durationError,
		attemptedSubmit,
		handleSubmit,
		authorName,
		setAuthorName,
		authorNameError,
		authors,
		courseAuthors,
		handleAddAuthor,
		handleDeleteAuthor,
		handleCourseAuthorRemove,
		handleAddToCourseAuthors,
		title,
		description,
		buttonLabel,
		headingName,
	} = useCourseForm({ navigateBack });

	const handleAddAuthorClick = () => {
		if (authorNameError) {
			return;
		}
		handleAddAuthor(authorName);
		setAuthorName('');
		console.log('Author name after clear:', authorName);
	};

	console.log('Authors in component:', authors);
	if (Array.isArray(authors)) {
		const filteredAuthors = authors.filter(
			(author) =>
				!courseAuthors.find((courseAuthor) => author.id === courseAuthor.id)
		);

		console.log('Filtered authors:', filteredAuthors);
	} else {
		console.error('Expected authors to be an array, but got:', authors);
	}
	console.log(
		'Data for CREATE COURSE rendering :',
		title,
		description,
		duration
	);

	return (
		<div className='createCourse'>
			<div className='createCourse-container'>
				<h1 className='createCourse-container-heading'>{headingName}</h1>
				<form className='createCourse-container-form'>
					<MainInfo
						handleChange={handleChange}
						attemptedSubmit={attemptedSubmit}
						titleError={titleError}
						descriptionError={descriptionError}
						title={title}
						description={description}
					/>
					<div className='createCourse-container-form-durationBox'>
						<div className='createCourse-container-form-durationBox-left'>
							<h2 className='createCourse-container-form-durationHeading'>
								Duration
							</h2>
							<Duration
								handleChange={handleChange}
								attemptedSubmit={attemptedSubmit}
								durationError={durationError}
								duration={duration}
							/>
							<div className='createCourse-container-form-authors'>
								<div className='createCourse-container-form-authors-leftSide'>
									<div className='createCourse-durationBox-left-authorBlock'>
										<h2 className='createCourse-durationBox-left-authorBlock-authorHeading'>
											Authors
										</h2>
										<AuthorNameInput
											authorName={authorName}
											handleChange={handleChange}
											handleButtonClick={handleAddAuthorClick}
											authorNameError={authorNameError}
											attemptedSubmit={attemptedSubmit}
											key={authors.length}
										/>
										<div className='authorName-item-list'>
											<h1 className='authorName-item-list-heading'>
												Authors List
											</h1>
											{authors
												.filter(
													(author) =>
														courseAuthors.find(
															(courseAuthors) => author.id === courseAuthors.id
														) === undefined
												)
												.map((author) => (
													<AuthorItem
														key={author.id}
														authorName={author.name}
														authorId={author.id}
														onAuthorAdded={handleAddToCourseAuthors}
														onAuthorRemoved={handleDeleteAuthor}
													/>
												))}
										</div>
									</div>
									<div className='createCourse-durationBox-left-authorsListBlock'></div>
								</div>
								<div className='createCourse-container-form-authors-rightSide'>
									<h1 className='createCourse-right-courseAuthors'>
										Course Authors
									</h1>
									<CourseAuthors
										authors={courseAuthors}
										onRemoveAuthor={handleCourseAuthorRemove}
									/>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div className='createCourse-container-footer'>
					<Button className='input-authorName-btn' onClick={navigateBack}>
						cancel
					</Button>
					<Button
						className='input-authorName-btn'
						type='button'
						onClick={handleSubmit}
					>
						{buttonLabel}
					</Button>
				</div>
			</div>
		</div>
	);
};
export default CourseForm;
