import React from 'react';
import Input from '../../../сommon/Input/input';

interface MainInfoProps {
	handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
	attemptedSubmit: boolean;
	titleError: string | null;
	descriptionError: string | null;
	title: string; // Убедитесь, что добавили это
	description: string; // Убедитесь, что добавили это
}

const MainInfo: React.FC<MainInfoProps> = ({
	handleChange,
	attemptedSubmit,
	titleError,
	descriptionError,
	title,
	description,
}) => {
	return (
		<div className='createCourse-container-form-mainBox'>
			<h2 className='createCourse-container-form-mainInfoHeading'>Main Info</h2>
			<Input
				htmlFor='title'
				labelText='Title'
				inputType='text'
				placeholderText='Enter title'
				onChange={handleChange}
				showError={attemptedSubmit && !!titleError}
				errorMessage={titleError}
				className='input-title'
				value={title}
			/>
			<Input
				htmlFor='description'
				labelText='Description'
				inputType='text'
				placeholderText='Enter description'
				onChange={handleChange}
				showError={attemptedSubmit && !!descriptionError}
				errorMessage={descriptionError}
				className='input-description'
				value={description}
			/>
		</div>
	);
};

export default MainInfo;
