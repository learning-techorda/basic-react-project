import React from 'react';
import Input from '../../../сommon/Input/input';
import getCourseDuration from '../../../helpers/getCourseDuration';

interface DurationProps {
	handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
	attemptedSubmit: boolean;
	durationError: string | null;
	duration: string;
}

const Duration: React.FC<DurationProps> = ({
	handleChange,
	attemptedSubmit,
	durationError,
	duration,
}) => {
	return (
		<div className='createCourse-durationBox-left-durationBlock'>
			<div className='durationBlock-input-container'>
				<label className='duration-label' htmlFor='duration'>
					Duration
				</label>
				<div className='input-with-converter'>
					<Input
						htmlFor='duration'
						inputType='number'
						placeholderText='Enter duration'
						onChange={handleChange}
						className={`input-duration ${attemptedSubmit && durationError ? 'error' : ''}`}
						min='1'
						max='999'
						value={duration}
					/>
					<p className='durationConverter'>
						{getCourseDuration(!duration ? '00:00' : duration)}
					</p>
				</div>
				{attemptedSubmit && durationError && (
					<div className='error-message'>{durationError}</div>
				)}
			</div>
		</div>
	);
};

export default Duration;
