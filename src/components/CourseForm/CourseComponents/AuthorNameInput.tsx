import React from 'react';
import Input from '../../../сommon/Input/input';
import Button from '../../../сommon/Button/Button';

interface AuthorNameInputProps {
	authorName: string;
	handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
	handleButtonClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
	authorNameError?: string;
	attemptedSubmit: boolean;
}

const AuthorNameInput: React.FC<AuthorNameInputProps> = ({
	authorName,
	handleChange,
	handleButtonClick,
	authorNameError,
	attemptedSubmit,
}) => {
	return (
		<>
			<label className='authorName-label' htmlFor='authorName'>
				Author Name
			</label>
			<div className='left-authorBlock-container'>
				<Input
					htmlFor='authorName'
					inputType='text'
					placeholderText='Input text'
					onChange={handleChange}
					className={`input-authorName ${attemptedSubmit && authorNameError ? 'error' : ''}`}
					name='authorName'
					type='text'
					value={authorName}
				/>
				<Button
					className='input-authorName-btn'
					onClick={handleButtonClick}
					type='button'
				>
					Create Author
				</Button>
			</div>
			{attemptedSubmit && authorNameError && (
				<div className='error-message'>{authorNameError}</div>
			)}
		</>
	);
};

export default AuthorNameInput;
