import { useEffect, useState } from 'react';
import { CourseType } from 'src/types/card';
import { addCourse, updateCourse } from '../../store/courses/thunk';
import {
	addAuthor,
	deleteAuthor,
	removeAuthor,
} from '../../store/authors/thunk';
import { RootState, AppDispatch } from '../../store';
import {
	composeValidators,
	durationValidator,
	isRequiredValidator,
	minLengthValidator,
} from 'src/helpers/validators';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

export interface Author {
	id: string;
	name: string;
}

interface UseCourseFormProps {
	navigateBack: () => void;
}

function useCourseForm({ navigateBack }: UseCourseFormProps) {
	const authors = useSelector((state: RootState) => state.authors);

	console.log('Authors in component:', authors);

	const [attemptedSubmit, setAttemptedSubmit] = useState(false);

	const [title, setTitle] = useState('');
	const [titleError, setTitleError] = useState('');

	const [description, setDescription] = useState('');
	const [descriptionError, setDescriptionError] = useState('');

	const [duration, setDuration] = useState('');
	const [durationError, setDurationError] = useState('');

	const [authorName, setAuthorName] = useState('');
	const [authorNameError, setAuthorNameError] = useState('');

	const [courseAuthors, setCourseAuthors] = useState<Author[]>([]);

	const { courseId } = useParams<{ courseId: string }>();
	const dispatch = useDispatch<AppDispatch>();
	const course = useSelector((state: RootState) => {
		if (!Array.isArray(state.courses)) {
			console.error('courses is not an array:', state.courses);
			return null;
		}
		return state.courses.find((c) => c.id === courseId);
	});
	useEffect(() => {
		const loadData = async () => {
			if (course && location.pathname.includes(`/courses/update/${courseId}`)) {
				setTitle(course.title);
				setDescription(course.description);
				setDuration(course.duration.toString());
				setCourseAuthors(
					course.authors.map((authorId: string) => {
						const author = authors.find((a: Author) => a.id === authorId);
						return {
							id: authorId,
							name: author ? author.name : 'Неизвестный',
						};
					})
				);
			}
		};

		loadData().catch((error) => {
			console.error('Failed to load data:', error);
		});
	}, [courseId, course, location.pathname, authors]);

	const handleChange = (event: { target: { name: string; value: string } }) => {
		const { name, value } = event.target;
		if (name === 'authorName') {
			setAuthorName(value);
		}

		switch (name) {
			case 'title':
				setTitle(value);
				setTitleError(
					composeValidators({
						name: 'Title',
						value,
						validators: [isRequiredValidator, minLengthValidator],
					})
				);
				break;
			case 'description':
				setDescription(value);
				setDescriptionError(
					composeValidators({
						name: 'Description',
						value,
						validators: [isRequiredValidator, minLengthValidator],
					})
				);
				break;
			case 'duration':
				setDuration(value);
				setDurationError(
					composeValidators({
						name: 'Duration',
						value,
						validators: [isRequiredValidator, durationValidator],
					})
				);
				break;
			case 'authorName':
				setAuthorName(value);
				setAuthorNameError(
					composeValidators({
						name: 'Author name',
						value,
						validators: [isRequiredValidator, minLengthValidator],
					})
				);
				break;
			default:
				break;
		}
	};

	const handleSubmit = async () => {
		setAttemptedSubmit(true);

		const errors: Record<string, string> = {};

		const tmpTitleError = composeValidators({
			name: 'Title',
			value: title,
			validators: [isRequiredValidator, minLengthValidator],
		});
		if (tmpTitleError) {
			errors.title = tmpTitleError;
			setTitleError(tmpTitleError);
		}

		const tmpDescriptionError = composeValidators({
			name: 'Description',
			value: description,
			validators: [isRequiredValidator, minLengthValidator],
		});
		if (tmpDescriptionError) {
			errors.description = tmpDescriptionError;
			setDescriptionError(tmpDescriptionError);
		}

		const tmpDurationError = composeValidators({
			name: 'Duration',
			value: duration,
			validators: [isRequiredValidator, durationValidator],
		});
		if (tmpDurationError) {
			errors.duration = tmpDescriptionError;
			setDurationError(tmpDurationError);
		}

		if (Object.keys(errors).length === 0) {
			const newCourse: CourseType = {
				title,
				description,
				duration: parseInt(duration, 10),
				authors: courseAuthors.map(({ id }) => id),
			};
			try {
				if (location.pathname.includes(`/courses/update/${courseId}`)) {
					await dispatch(
						updateCourse({ courseId: courseId as string, course: newCourse })
					);
				} else if (location.pathname === '/courses/add') {
					await dispatch(addCourse(newCourse));
				}
				navigateBack();
			} catch (error) {
				console.error('Failed to add course:', error);
			}
		} else {
			console.error('Form validation failed.');
		}
	};
	// dispatch(updateCourseThunk({ id: course.id, course: courseData }));
	const handleAddAuthor = async (authorName: string) => {
		try {
			await dispatch(addAuthor(authorName)).unwrap();
		} catch (error) {
			console.error('Failed to add author:', error);
		}
	};

	const handleAddToCourseAuthors = (authorId: string) => {
		const author = authors.find(
			(author: { id: string }) => author.id === authorId
		);
		if (author) {
			setCourseAuthors((prev) => [...prev, author]);
		}
	};

	const handleDeleteAuthor = async (id: string) => {
		try {
			await dispatch(deleteAuthor(id));
			dispatch(removeAuthor(id));
		} catch (error) {
			console.error('Failed to delete author:', error);
		}
	};

	const handleCourseAuthorRemove = (authorId: string) => {
		setCourseAuthors((prev) => prev.filter((val) => val.id !== authorId));
	};

	const buttonLabel = location.pathname.includes(`/courses/update/${courseId}`)
		? 'Update Course'
		: location.pathname === '/courses/add'
			? 'Create Course'
			: 'Submit';

	const headingName = location.pathname.includes(`/courses/update/${courseId}`)
		? 'Course edit'
		: location.pathname === '/courses/add'
			? 'Create page'
			: 'Course edit/create page';

	return {
		title,
		setTitle,
		titleError,
		description,
		setDescription,
		descriptionError,
		duration,
		authors,
		courseAuthors,
		handleAddAuthor,
		handleAddToCourseAuthors,
		handleDeleteAuthor,
		handleCourseAuthorRemove,
		setDuration,
		durationError,
		handleChange,
		handleSubmit,
		attemptedSubmit,
		authorName,
		setAuthorName,
		authorNameError,
		setAuthorNameError,
		buttonLabel,
		headingName,
	};
}

export default useCourseForm;
