import React, { FC } from 'react';
import Button from 'src/сommon/Button/Button';
import plus from './images-btn/plus-solid.svg';
import trash from './images-btn/trash-can-regular.svg';
interface AuthorItemProps {
	authorName: string;
	authorId: string;
	onAuthorAdded: (id: string) => void;
	onAuthorRemoved: (id: string) => void;
}

const AuthorItem: FC<AuthorItemProps> = ({
	authorName,
	authorId,
	onAuthorRemoved,
	onAuthorAdded,
}) => {
	const handleAddClick = () => {
		onAuthorAdded(authorId);
	};

	const handleDeleteClick = () => {
		onAuthorRemoved(authorId);
	};

	return (
		<div className='author-item'>
			<span className='author-item-names'>{authorName || 'No name'}</span>
			<Button
				className='author-item-btn-add'
				type='button'
				onClick={handleAddClick}
				imagePath={plus}
				altText='Move Author'
			></Button>
			<Button
				className='author-item-btn-delete'
				type='button'
				onClick={handleDeleteClick}
				imagePath={trash}
				altText='Move Author'
			></Button>
		</div>
	);
};
export default AuthorItem;
