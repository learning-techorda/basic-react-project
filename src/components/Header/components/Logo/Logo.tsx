import react from 'react';
import logo from './images/logo.png';

function Logo(): react.ReactElement {
	return <img className='header-logo' src={logo} alt='logo-header' />;
}

export default Logo;
