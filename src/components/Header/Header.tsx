import React from 'react';
import Button from '../../сommon/Button/Button';
import Logo from './components/Logo/Logo';
import { useNavigate, useLocation } from 'react-router-dom';
import './Header.sass';
import { useAuth } from '../../AuthContext';
import { AC } from '../AC/AC';
import { useDispatch, useSelector } from 'react-redux';
import { logout as reduxLogout } from '../../store/user/actions';
import { RootState } from 'src/store';

function Header(): React.ReactElement {
	const { user, logout } = useAuth();
	const navigate = useNavigate();
	const location = useLocation();

	const dispatch = useDispatch();

	const handleLoginLogout = async () => {
		if (user) {
			try {
				const userToken = localStorage.getItem('userToken') ?? undefined;
				const response = await fetch('http://localhost:4000/logout', {
					method: 'DELETE',
					headers: {
						'Content-Type': 'application/json',
						'Cache-Control': 'no-cache',
						...(userToken ? { Authorization: userToken } : {}),
					},
				});
				if (!response.ok) {
					throw new Error('Failed to logout');
				}
				console.log('Logout successful');
				dispatch(reduxLogout());
				logout();
				navigate('/login');
				console.log('Logging out');
			} catch (error) {
				console.error('Logout error:', error);
			}
		} else {
			console.log('User not logged in, redirecting to login');
			navigate('/login');
		}
	};

	const showLoginButton = !['/login', '/registration'].includes(
		location.pathname
	);

	const userName = useSelector((state: RootState) => state.user.name);

	return (
		<nav className='navbar'>
			<Logo />
			<div className='right-section'>
				<AC>
					<div className='header-name'>{userName ? userName : 'No user'}</div>
				</AC>
				{showLoginButton && (
					<Button
						onClick={handleLoginLogout}
						className='common-btn-style btn-logo'
					>
						{user ? 'LOGOUT' : 'LOGIN'}
					</Button>
				)}
			</div>
		</nav>
	);
}

export default Header;
