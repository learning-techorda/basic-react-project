import React, { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import { AuthorsType, CourseType } from '../../types/card';
import Button from 'src/сommon/Button/Button';
import './Courses.sass';
import { AC } from '../AC/AC';
import { deleteCourse } from 'src/store/courses/thunk';
import { useDispatch } from 'react-redux';
import { AppDispatch } from 'src/store';
import { PrivateRouteButton } from '../PrivateRoute/PrivateRouteButton';

interface CourseProp {
	courses: CourseType[];
	authors: AuthorsType[];
	onAuthorRemoved: (id: string) => void;
}

const Courses: FC<CourseProp> = ({ courses, authors }) => {
	const navigate = useNavigate();

	const dispatch: AppDispatch = useDispatch();

	const handleRemoveCourse = (courseId: string) => {
		console.log('Attempting to remove course with ID:', courseId);
		if (courseId) {
			dispatch(deleteCourse(courseId));
		} else {
			console.error('Course ID is missing');
		}
	};

	const renderCourse = (course: CourseType) => (
		<CourseCard
			key={course.id}
			courseId={course.id!}
			course={course}
			authors={authors.filter((author) => course.authors.includes(author.id))}
			onRemoveCard={handleRemoveCourse}
		/>
	);

	const handleAddNewCourseClick = () => {
		navigate('/courses/add');
	};

	return (
		<div>
			<div className='courses-container'>
				<div className='SearchBar'>
					<SearchBar />
					<AC>
						<PrivateRouteButton>
							<Button
								className='btn-addCourse'
								onClick={handleAddNewCourseClick}
							>
								ADD NEW COURSE
							</Button>
						</PrivateRouteButton>
					</AC>
				</div>
			</div>
			{courses.map(renderCourse)}
		</div>
	);
};

export default Courses;
