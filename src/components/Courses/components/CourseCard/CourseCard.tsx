import React from 'react';
import Button from '../../../../сommon/Button/Button';
import { useNavigate } from 'react-router-dom';
// import formatCreationDate from '../../../../helpers/formatCreationDate';
import getCourseDuration from '../../../../helpers/getCourseDuration';
import { AuthorsType, CourseType } from '../../../../types/card';
import './CourseCard.sass';
import { AC } from '../../../AC/AC';
import trash from '../../../AuthorItem/images-btn/trash-can-regular.svg';
import pencil from './CourseCard-img/pencil-solid.svg';
import { PrivateRouteButton } from '../../../PrivateRoute/PrivateRouteButton';
import formatCreationDate from '../../../../helpers/formatCreationDate';

interface CourseCardProps {
	courseId: string;
	course: CourseType;
	authors: AuthorsType[];
	onRemoveCard: (courseId: string) => void;
}

const CourseCard: React.FC<CourseCardProps> = ({
	courseId,
	course,
	authors,
	onRemoveCard,
}) => {
	const navigate = useNavigate();

	const handleDeleteCourse = () => {
		console.log('Delete button clicked for course ID:', courseId);
		onRemoveCard(courseId);
	};

	if (!course) {
		return <div>Course not found</div>;
	}

	const authorNames = course.authors
		.map(
			(authorId: string) =>
				authors.find((author: { id: string }) => author.id === authorId)
					?.name || 'Unknown'
		)
		.join(', ');

	const handleShowCourseBtnClick = () => {
		navigate(`/courses/${courseId}`);
	};

	const handleUpdateCourse = () => {
		navigate(`/courses/update/${courseId}`);
	};

	return (
		<div className='CourseCard'>
			<div className='CourseCard-container'>
				<div className='CourseCard-header'>
					<h2 className='CourseCard-header-title'>{course.title}</h2>
				</div>
				<div className='CourseCard-body'>
					<div className='CourseCard-body-description'>
						<p className='CourseCard-body-description-text'>
							{course.description}
						</p>
					</div>
					<div className='CourseCard-body-data'>
						<ul>
							<li>
								<strong>Authors:</strong> {authorNames}
							</li>
							<li>
								<strong>Duration:</strong> {getCourseDuration(course.duration)}
							</li>
							<li>
								<strong>Created:</strong>{' '}
								{formatCreationDate(course.creationDate)}
							</li>
						</ul>
						<Button
							className='common-btn-style btn-card'
							onClick={handleShowCourseBtnClick}
						>
							SHOW COURSE
						</Button>
						<AC>
							<PrivateRouteButton>
								<Button
									className='common-btn-style btn-trash'
									type='button'
									onClick={handleDeleteCourse}
									altText='Delete Course'
								>
									<img
										src={trash}
										alt='Delete Course'
										style={{
											width: '24px',
											height: '24px',
											filter: 'invert(100%)',
										}}
									/>
								</Button>
								<Button
									className='common-btn-style btn-pencil'
									type='button'
									onClick={handleUpdateCourse}
									altText='Update Course'
								>
									<img
										src={pencil}
										alt='Update Course'
										style={{
											width: '24px',
											height: '24px',
											filter: 'invert(100%)',
										}}
									/>
								</Button>
							</PrivateRouteButton>
						</AC>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CourseCard;
