/* eslint-disable @typescript-eslint/no-empty-function */
import React from 'react';
import Button from '../../../../сommon/Button/Button';

function SearchBar() {
	return (
		<div className='SearchBar-container'>
			<input
				className='SearchBar-input'
				type='text'
				placeholder='Input text'
			></input>
			<Button className='btn-SearchBar' onClick={() => {}}>
				SEARCH
			</Button>
		</div>
	);
}
export default SearchBar;
