const getCourseDuration = (minutes: number | string): string => {
	const totalMinutes =
		typeof minutes === 'string' ? parseInt(minutes, 10) : minutes;

	const hours: number = Math.floor(totalMinutes / 60);
	const mins: number = totalMinutes % 60;

	const formattedHours: string = hours < 10 ? `0${hours}` : hours.toString();
	const formattedMins: string = mins < 10 ? `0${mins}` : mins.toString();
	const hourText: string = hours === 1 ? 'hour' : 'hours';

	return `${formattedHours}:${formattedMins} ${hourText}`;
};

export default getCourseDuration;
