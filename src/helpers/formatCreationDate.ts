import { format, parseISO } from 'date-fns';

const formatCreationDate = (dateString: string | undefined): string => {
	if (!dateString) {
		return 'No data';
	}
	const result = parseISO(dateString);
	return format(result, 'dd/MM/yyyy');
};

export default formatCreationDate;
