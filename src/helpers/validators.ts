type Validator = (name: string, value?: string) => string;

export const composeValidators = ({
	name,
	value,
	validators,
}: {
	name: string;
	value?: string;
	validators: Array<Validator>;
}) => {
	for (const validator of validators) {
		const result = validator(name, value);

		if (result) return result;
	}

	return '';
};

export const isRequiredValidator: Validator = (
	name: string,
	value?: string
) => {
	if (!value?.trim()) {
		return `${name[0].toUpperCase() + name.slice(1)} is required.`;
	}
	return '';
};

export const minLengthValidator: Validator = (name: string, value?: string) => {
	if (!value?.trim() || value?.trim()?.length < 2) {
		return `${name[0].toUpperCase() + name.slice(1)} should be more than 2 symbols.`;
	}
	return '';
};

export const durationValidator: Validator = (name: string, value?: string) => {
	if (Number(value) === 0) {
		return `Duration should be more than 0 minutes`;
	}
	return '';
};

export const validateField = (name: string, value: string) => {
	if (!value.trim() || value.trim().length < 2) {
		return `${name[0].toUpperCase() + name.slice(1)} is required.`;
	}
	return '';
};
